package macknight.com.br.mackaplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
String jsonAddress = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=\"São%20Carlos,%20SP\")&format=json&env=store://datatables.org/alltableswithkeys";

public static String jsonFromYahoo  = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


      HttpGetRequest requester = new HttpGetRequest();
      requester.execute(jsonAddress);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if(jsonFromYahoo!=null){

                    jsonFromYahoo = jsonFromYahoo.substring(jsonFromYahoo.indexOf("forecast")+1);

                    String result = jsonFromYahoo.split(",\"description")[0];
                    Gson gson = new GsonBuilder().create();


                    Type listType = new TypeToken<ArrayList<Forecast>>(){}.getType();
                    List<Forecast> yourClassList = new Gson().fromJson(result, listType);



                    System.out.println(result);
                }
            }
        }, 3000);




    }

    public class HttpGetRequest extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;
        @Override
        protected String doInBackground(String... params){
            String stringUrl = params[0];
            String result ="";
            String inputLine;
            try {
                //Create a URL object holding our url
                URL myUrl = new URL(stringUrl);
                /* Create a connection */
                HttpURLConnection connection =(HttpURLConnection)
                        myUrl.openConnection();
                //Set methods and timeouts


                //Connect to our url
                connection.connect();
                //Create a new InputStreamReader
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                //Check if the line we are reading is not null
                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
                jsonFromYahoo = result;
            }
            catch(IOException e){
                e.printStackTrace();


            }
            return result;
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            jsonFromYahoo = result;
        }
    }
//git@bitbucket.org:<repo_owner>/<repo_name>.git
    //https://Macknight@bitbucket.org/Macknight/weatherapp.git
    //https://bitbucket.org/Macknight/weatherapp.git
    //git remote add origin https://Macknight@bitbucket.org/weatherapp/repo.git


}
